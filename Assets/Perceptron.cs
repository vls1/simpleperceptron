﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PerseptronTestData 
{
    public string name;
    public List<int> inputValues = new List<int>();
    public int outputValue;
}

public class Perceptron : MonoBehaviour
{
    public List<PerseptronTestData> testData = new List<PerseptronTestData>();

    //Переложить в Unity логику консольного перцептрона
    //Вывод реализовать с помощью Debug.Log
}
