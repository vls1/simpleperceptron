﻿using System;

namespace PerceptronSample
{
    class Perceptron
    {
        static void Main(string[] args)
        {
            int[,] inputValues = { { 0, 0 },{ 1, 0 },{ 0, 1 },{ 1, 1 } };
            int[] outputValues = { 0, 0, 0, 1 };

            Random rnd = new Random();
            double[] weights = { GetRandomDouble(-1,1), GetRandomDouble(-1, 1) };
            double bias = GetRandomDouble(-1, 1);

            double error = 1;
           
            while (error > 0.5) 
            {
                error = 0;
                for (int i = 0; i < 4; i++)
                {
                    int output = CalculateOutput(inputValues[i, 0], inputValues[i, 1], bias, weights);
                    int localError = outputValues[i] - output;

                    weights[0] = inputValues[i, 0] * localError + weights[0];
                    weights[1] = inputValues[i, 1] * localError + weights[1];
                    bias += localError;

                    error += Math.Abs(localError);

                    Console.WriteLine($"W1 {weights[0]}, W2{weights[1]}, B{bias}");
                    Console.WriteLine($"Error: {error}");
                    Console.WriteLine("-------------------------------------------------------------------------------------------------");
                    

                }
                Console.WriteLine("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
            }

            Console.ReadKey();
        }

        private static int CalculateOutput(double input1, double input2, double bias, double[] weignts) 
        {
            double result = input1 * weignts[0] + input2 * weignts[1] + bias;
            Console.WriteLine($"Result = {input1 * weignts[0]} + {input2 * weignts[1]} + {bias}");
            return (result >= 0) ? 1 : 0;
        }

        private static double GetRandomDouble(double min, double max) 
        {
            Random rnd = new Random();
            return rnd.NextDouble() * (max - min) + min;
        }
    }
}
